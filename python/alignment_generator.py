#!/usr/bin/python

import fst
import itertools

def main():
    french_length = 3
    english_length = 3
    
    french = []
    english = []
    for i in range(english_length):
        english.append("e" + str(i))
        
    for i in range(french_length):
        french.append("f" + str(i))


    
    # f is a linear chanin machine that we will *not* build
    # D is a distortion machine that we will also *not* build
    # rather we build U = f o D


    french_symbol_table = fst.SymbolTable()
    english_symbol_table = fst.SymbolTable()
    alignment_symbol_table = fst.SymbolTable()

    for i,f in enumerate(french):
        french_symbol_table[f] = i+1
    for i,e in enumerate(english):
        english_symbol_table[e] = i+1
    
    for i,a in enumerate(itertools.product(french,english)):
        alignment_symbol_table[a[0] + ":" + a[1]] = i + 1

    U = fst.LogTransducer()

    #adds states
    for i in range(french_length * english_length):
        U.add_state()

    
    next_state = {}
    counter = 1
    for i in range(english_length + 1):
        for f in french:
            if f not in next_state:
                next_state[f] = {}
            next_state[f][i] = counter
            counter += 1

    
    #add arc
    for i in range(len(french)):
        for f1 in french :
            for f2 in french:
                
               # for e in english:
                U.add_arc(next_state[f1][i],next_state[f2][i+1],f1,f2 + ":e" + str(i),0.0)


    for f in french:
        U.add_arc(0,next_state[f][0],fst.EPSILON,fst.EPSILON,0.0)
        U[next_state[f][english_length]].final = True


    print "DONE"


    U.write("U.fst",french_symbol_table, alignment_symbol_table)

    E = fst.LogTransducer()
    for f in french:
        for e in english:
            E.add_arc(0,0,f,e,0.0)

    E.write("E.fst",french_symbol_table, english_symbol_table)
    E[0].final = True
    
    M = U.compose(E)
    M.write("M.fst",french_symbol_table, english_symbol_table)

    eng = fst.LogTransducer()
    for i in english:
        eng.add_state()
    eng[len(english)].final = True

    for i,e in enumerate(english):
        eng.add_arc(i,i+1,e,e,0.0)

    eng.write("eng.fst",english_symbol_table,english_symbol_table)

    final = M.compose(eng)
    final.write("final.fst",french_symbol_table,english_symbol_table)

if __name__ == "__main__":
    main()
