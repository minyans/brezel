#!/bin/bash


fstdraw $1 tmp1.dot
cat tmp1.dot | python utils/add_state_label.py $2 $3 > tmp.dot
dot -Tps tmp.dot -o tmp.ps
open tmp.ps
rm tmp.dot
rm tmp1.dot
rm tmp.ps
