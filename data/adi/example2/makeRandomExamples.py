__author__ = 'arenduchintala'
from random import random
import fst
import itertools as it

phrase_syms = ['A_A', 'A_B', 'B_A', 'B_B', 'C_A', 'C_B']
input_syms = ['A', 'B', 'C']
sigma = phrase_syms + input_syms


def sample_wr(population, k):
    "Chooses k random elements (with replacement) from a population"
    n = len(population)
    _random, _int = random, int  # speed hack
    result = [None] * k
    for i in xrange(k):
        j = _int(_random() * n)
        result[i] = population[j]
    return result


def map_to_output(seq):
    result = []
    # for idx, s in enumerate(seq):
    i = 0
    while i < len(seq):
        s0 = seq[i - 1] if i - 1 >= 0 else ''
        s1 = seq[i]
        s2 = seq[i + 1] if i + 1 < len(seq) else ''
        s3 = seq[i + 2] if i + 2 < len(seq) else ''
        if (s1 + '_' + s2 in phrase_syms) and (s0 == s1 or s2 == s3):
            result.append(s1 + '_' + s2)
            i += 2
        else:
            result.append(s1)
            i += 1

    return result


def linear_chain(txt, syms):
    lc = fst.LogTransducer(syms, syms)
    for idx, t in enumerate(txt):
        n = lc.add_state()
        lc.add_arc(idx, n, t, t, 0.0)
    lc[n].final = True
    return lc


def T(syms):
    seg = fst.LogTransducer(syms, syms)
    for s in input_syms:
        seg.add_arc(0, 0, s, s, 0.0)
    for s1, s2 in it.product(input_syms, input_syms):
        sid1 = seg.add_state()
        seg.add_arc(0, sid1, s1, fst.EPSILON, 0.0)
        sid2 = seg.add_state()
        seg.add_arc(sid1, sid2, s2, fst.EPSILON, 0.0)
        if s1 + '_' + s2 in phrase_syms:
            seg.add_arc(sid2, 0, fst.EPSILON, s1 + '_' + s2, 0.0)
        sid3 = seg.add_state()
        seg.add_arc(sid2, sid3, fst.EPSILON, s1, 0.0)
        seg.add_arc(sid3, 0, fst.EPSILON, s2)
    seg[0].final = True
    return seg


if __name__ == '__main__':
    syms = fst.SymbolTable()
    for s in sigma:
        syms[s]
    syms.write('syms.bin')
    for n in xrange(5):
        input_seq = sample_wr(input_syms, 10)
        output_seq = map_to_output(input_seq)
        print input_seq, output_seq
        in_fst = linear_chain(input_seq, syms)
        in_fst.write('in' + str(n) + '.fst', syms, syms)
        out_fst = linear_chain(output_seq, syms)
        out_fst.write('out' + str(n) + '.fst', syms, syms)
    seg = T(syms)
    seg.write('T.fst', syms, syms)

    f = open('features', 'w')
    w = open('weights', 'w')
    idx = 0
    for s in seg.states:
        for a in s.arcs:
            f.write(str(idx) + '\t' + str(idx) + '\n')
            w.write(str(idx) + '\t0.0\n')
            idx+=1
    f.flush()
    f.close()
    w.flush()
    w.close()
