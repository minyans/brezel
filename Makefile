CC= g++ 


OBJS=	data-io.o wfst-train.o wfst-train-global.o batch_selector.o lbfgs.o
#wfst-train-local.o 
MAIN = global_toy
INC= -ldl -lfst -lpthread -Ilib/dlib-18.7 -Isrc -g -pg



all:    $(OBJS) src/$(MAIN).cpp
	 $(CC) $(INC)  $(OBJS)  -o $(MAIN) src/$(MAIN).cpp
		strip $(MAIN)	

batch_selector.o:  src/batch_selector.cpp
	$(CC) $(INC) -c src/batch_selector.cpp -o batch_selector.o

data-io.o: src/data-io.cpp
	$(CC) $(INC) -c src/data-io.cpp -o data-io.o

wfst-train.o: data-io.o src/wfst-train.cpp
	$(CC) $(INC) -c src/wfst-train.cpp -o wfst-train.o

wfst-train-global.o: wfst-train.o src/wfst-train-global.cpp
	$(CC) $(INC) -c src/wfst-train-global.cpp -o wfst-train-global.o

#wfst-train-local.o: wfst-train.o src/wfst-train-local.cpp
#	$(CC) $(INC) -c src/wfst-train-local.cpp -o wfst-train-local.o

lbfgs.o: 
	$(CC) $(INC) -c src/lbfgs.cpp -o lbfgs.o

test:

clean:
	rm $(OBJS) 
	rm $(MAIN)

