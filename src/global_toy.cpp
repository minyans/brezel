// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#ifndef GLOBAL_TOY_H
#define GLOBAL_TOY_H

#include "wfst-train-global.h"
#include "regularizer.h"
#include "batch_selector.h"
#include "lbfgs.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[]) {
  string sample_machine = argv[1];

  FeatureReader fr = FeatureReader(argv[2]);

  feature_map features = fr.get_features();

  WeightReader wr = WeightReader(argv[3]);
  feature_weights weights = wr.get_weights();
  string path = string(argv[4]);
  cout<<path<<"is the path"<<endl;
  vector<string> str_vec;
  split(str_vec, path, boost::is_any_of("/"));
  str_vec.pop_back();
  string joined = boost::algorithm::join(str_vec, "/"); 
  ifstream ifs(path.c_str());
  string line;
  int i =0;
  int exemplars = 0;  
  vector<string> upper_fst(0);
  vector<string> lower_fst(0);
  while (getline(ifs, line)) {
    if (i==0){
        cout<<"number of files"<<line<<endl;
        lower_fst.resize(atoi(line.c_str()));
        upper_fst.resize(atoi(line.c_str()));
        exemplars = atoi(line.c_str());
    }else{
        vector<string> v;
        split(v, line, boost::is_any_of("\t"));
        string trelis = string(joined)+string("/") + string(v[0]);
        string y = string(joined) + string("/")+ string(v[1]);
        cout<<"trelis:"<<trelis<<"\t"<<"y:"<<y<<endl;
        upper_fst[i-1] = trelis;
        lower_fst[i-1] = y;
    }
    i++;
  } 
  cout<<"done with loading list"<<endl;  
  /*
  for (vector<double>::iterator it = weights.begin(); it != weights.end(); ++it) {
    cout << *it << "\n";
  }
  for (feature_map::iterator it = features.begin(); it != features.end(); ++it ) {
    vector<int> tmp = *it;
    
    for (vector<int>::iterator it2 = tmp.begin(); it2 != tmp.end(); ++it2) {
      cout << *it2 << "\n";
    }
    cout << "\n";
    }*/
  
 /* int exemplars = 10;
  vector<string> upper_fst(exemplars);
  upper_fst[0] = "data/alignment/toy2/model1/0.trelis.fst";
  upper_fst[1] = "data/alignment/toy2/model1/1.trelis.fst";
  upper_fst[2] = "data/alignment/toy2/model1/2.trelis.fst";
  upper_fst[3] = "data/alignment/toy2/model1/3.trelis.fst";
  upper_fst[4] = "data/alignment/toy2/model1/4.trelis.fst";
  upper_fst[5] = "data/alignment/toy2/model1/5.trelis.fst";
  upper_fst[6] = "data/alignment/toy2/model1/6.trelis.fst";
  upper_fst[7] = "data/alignment/toy2/model1/7.trelis.fst";
  upper_fst[8] = "data/alignment/toy2/model1/8.trelis.fst";
  upper_fst[9] = "data/alignment/toy2/model1/9.trelis.fst";

  vector<string> lower_fst(exemplars);

  lower_fst[0] = "data/alignment/toy2/model1/0.y.fst";
  lower_fst[1] = "data/alignment/toy2/model1/1.y.fst";
  lower_fst[2] = "data/alignment/toy2/model1/2.y.fst";
  lower_fst[3] = "data/alignment/toy2/model1/3.y.fst";
  lower_fst[4] = "data/alignment/toy2/model1/4.y.fst";
  lower_fst[5] = "data/alignment/toy2/model1/5.y.fst";
  lower_fst[6] = "data/alignment/toy2/model1/6.y.fst";
  lower_fst[7] = "data/alignment/toy2/model1/7.y.fst";
  lower_fst[8] = "data/alignment/toy2/model1/8.y.fst";
  lower_fst[9] = "data/alignment/toy2/model1/9.y.fst";
*/  
  VectorFst<LogArc> *fst = VectorFst<LogArc>::Read(sample_machine);
  int num_arcs = WFST_Trainer::get_number_arcs(fst);
  int num_features = fr.get_number_features();

  vector<datum_formatted> ps(upper_fst.size());

  for (int i = 0; i < upper_fst.size(); ++i) {
    VectorFst<LogArc> *upper_in = VectorFst<LogArc>::Read(upper_fst[i]);
    VectorFst<LogArc> *lower_in = VectorFst<LogArc>::Read(lower_fst[i]);
    datum_formatted p(*upper_in,*lower_in);
    ps[i] = p;
  }
  

  
  //Regularizer regularizer;
  Selector *selector = new Batch_Selector(ps.size());
  
  double lambda = .5;
  Optimizer *optimizer = new LBFGS();
  //Regularizer *regularizer = new Ridge(lambda);
  
  vector<int> epoch;
  selector->get_next_set(epoch);
  cout << "CONSTRUCTING WFST TRAINER OBJECT" << endl;

  vector<VectorFst<LogArc>*> fsts;
  vector<int> fst_unique_ids;

  cout << ps.size() << endl; 
  for (int i = 0; i < ps.size(); ++i) {
    //cout << i << "\n";
    fsts.push_back(fst);
    fst_unique_ids.push_back(0);
  }

  
  int num_unique_fsts = 1;
  cout << "THE TRAINER!!!\n";


  const SymbolTable *input_symbols = fst->InputSymbols();
  const SymbolTable *output_symbols = fst->OutputSymbols();
  WFST_Trainer *wfst_trainer = new WFST_Trainer_Global(fsts,fst_unique_ids, num_unique_fsts,*input_symbols,*output_symbols, ps,selector,optimizer, num_arcs, num_features, 100, 1, features,weights,true);
  


  wfst_trainer->train(5,1e-1,1,1e-1);


  delete wfst_trainer;
  delete optimizer;
}

#endif
