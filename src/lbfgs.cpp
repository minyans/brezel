// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//



#include "lbfgs.h"

/*
LBFGS::minimize(Gradient_Trainer *gt) {
  Value value(gt);
  Gradient gradient(gt);
  
  dlib::find_min(dlib::lbfgs_search_strategy(10),
		 dlib::objective_delta_stop_strategy(inner_threshold,inner_max_iterations),  
		 value, gradient,
		 tmp_weights, -1);
  
}
*/
LBFGS::LBFGS() {

}

void LBFGS::optimize(Value value, Gradient gradient, column_vector &x )  {
	double inner_threshold = 1e-4;
	double inner_max_iterations = 5;
   	dlib::find_max(dlib::lbfgs_search_strategy(10),
  		dlib::objective_delta_stop_strategy(inner_threshold,inner_max_iterations).be_verbose(),  
  		value, gradient,
  		x, -1);     
}


