// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

/*
 compose_benchmark/contextEdit_logsemiring.fst
compose_benchmark/lower_expectation.fst
compose_benchmark/lower_logsemiring.fst
compose_benchmark/observed_expectaton.fst
compose_benchmark/upper_expectation.fst
compose_benchmark/upper_logsemiring.fst
*/

#include "wfst-train.h"

int main(int argc, char* argv[]) {
  string sample_machine = argv[1];
  // string sample_symbol_table = argv[2];

  FeatureReader fr = FeatureReader(argv[2]);
  //WeightReader wr = WeightReader(argv[4]);

  feature_map features = fr.get_features();
  // feature_weights weights = wr.get_weights();

  VectorFst<LogArc> *fst = VectorFst<LogArc>::Read(sample_machine);


  int exemplars = 4;
  vector<string> upper_fst(exemplars);
  for (int i = 0; i < exemplars; ++i)  {
    string part1 = "/export/common/git/Phonology/wfst-phonology/underline_";
    string part2 = "_downmsg_iter0.fst";
    stringstream ss;
    ss << i;
    upper_fst[i] = part1 + ss.str() + part2;
  }
  
  vector<string> lower_fst(exemplars);

  for (int i = 0;i < exemplars; ++i) {
    string part1 = "/export/common/git/Phonology/wfst-phonology/surface_";
    string part2 = "upmsg_iter0.fst";
    stringstream ss;
    ss << i;
    lower_fst[i] = part1 + ss.str() + part2;
  }


  

  vector<datum_formatted> ps(exemplars);

  for (int i = 0; i < exemplars; ++i) {
    VectorFst<StdArc> *upper_in = VectorFst<StdArc>::Read(upper_fst[i]);
    VectorFst<LogArc> *lower_in = VectorFst<LogArc>::Read(lower_fst[i]);
    VectorFst<LogArc> *upper = new VectorFst<LogArc>();
    ArcMap(*upper_in,upper,Map_SL());
  
    /*
    upper_in->SetInputSymbols(fst->InputSymbols());
    upper_in->SetOutputSymbols(fst->OutputSymbols());

    
    lower->SetInputSymbols(fst->InputSymbols());
    lower->SetOutputSymbols(fst->OutputSymbols());
    */
    datum_formatted p(upper,lower_in);
    ps[i] = p;
  }


  feature_weights weights;
  int num_features = 38664;
  for (int i = 0; i < num_features; ++i) {
    weights.push_back(2 * i);
  }


  WFST_Trainer wfst_trainer = WFST_Trainer(fst,ps,38664, num_features, 100, 4, features,weights,false);
 
  wfst_trainer.write("untrained.fst");
  wfst_trainer.train(5,1,1e-1);
  wfst_trainer.write("trained.fst");

}
