// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#ifndef ALIGNER_H
#define ALIGNER_H

#include "wfst-train-global.h"
#include "batch_selector.h"
#include "lbfgs.h"
#include <iostream>
#include <fstream>

int main(int argc, char* argv[]) {
  string sample_machine = argv[1];

  FeatureReader fr = FeatureReader(argv[2]);

  feature_map features = fr.get_features();

  WeightReader wr = WeightReader(argv[3]);
  feature_weights weights = wr.get_weights();
  string path = string(argv[4]);
  cout<<path<<"is the path"<<endl;
  vector<string> str_vec;
  split(str_vec, path, boost::is_any_of("/"));
  str_vec.pop_back();
  string joined = boost::algorithm::join(str_vec, "/"); 
  ifstream ifs(path.c_str());
  string line;
  int i =0;
  int exemplars = 0;  
  vector<string> upper_fst(0);
  vector<string> lower_fst(0);
  while (getline(ifs, line)) {
    if (i==0){
        cout<<"number of files"<<line<<endl;
        lower_fst.resize(atoi(line.c_str()));
        upper_fst.resize(atoi(line.c_str()));
        exemplars = atoi(line.c_str());
    }else{
        vector<string> v;
        split(v, line, boost::is_any_of("\t"));
        string trelis = string(joined)+string("/") + string(v[0]);
        string y = string(joined) + string("/")+ string(v[1]);
        cout<<"trelis:"<<trelis<<"\t"<<"y:"<<y<<endl;
        upper_fst[i-1] = trelis;
        lower_fst[i-1] = y;
    }
    i++;
  } 
  cout<<"done with loading list"<<endl;  

  VectorFst<LogArc> *fst = VectorFst<LogArc>::Read(sample_machine);
  int num_arcs = WFST_Trainer::get_number_arcs(fst);
  int num_features = fr.get_number_features();

  vector<datum_formatted> ps(upper_fst.size());

  for (int i = 0; i < upper_fst.size(); ++i) {
    VectorFst<LogArc> *upper_in = VectorFst<LogArc>::Read(upper_fst[i]);
    VectorFst<LogArc> *lower_in = VectorFst<LogArc>::Read(lower_fst[i]);
    datum_formatted p(*upper_in,*lower_in);
    ps[i] = p;
  }
  

  int number_sentences = 10;
  vector<WFST_Trainer*> trainers;
  vector<Optimizer*> optimizers;
  vector<Selector*> selectors;

  for (int i = 0; i < number_sentences; ++i) {
    selectors.push_back(new Batch_Selector(ps.size()));
    optimizers.push_back(new LBFGS());
    trainers.push_back(new WFST_Trainer_Global(fst,ps,selectors[i],optimizers[i], num_arcs, num_features, 100, 1, features,weights,true));
  }


  wfst_trainer->train(5,1e-1,1,1e-1);

  for (int i = 0; number_sentences; ++i) {
    delete optimizers[i];
    delete selectors[i];
    delete trainers[i];
  }

}

#endif
