// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include <pthread.h>
#include <iostream>
#include <fstream>

#include <string>
#include <set>
#include <cmath>
#include <cfloat>
#include <stdlib.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <time.h>
#include <algorithm>

using namespace std;

class Optimizer { 
  
 public:
 virtual void optimize(Value value, Gradient gradient,  column_vector &x )  { }
};


#endif
