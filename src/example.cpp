// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//


#include "wfst-train.h"

int main(int argc, char* argv[]) {
  string sample_machine = argv[1];
  string sample_symbol_table = argv[2];

  DataReader dr = DataReader(argv[3]);
  FeatureReader fr = FeatureReader(argv[4]);
  WeightReader wr = WeightReader(argv[5]);

  training_data data = dr.get_data();
  feature_map features = fr.get_features();
  feature_weights weights = wr.get_weights();

  SymbolTable *symbol_table = SymbolTable::Read(sample_symbol_table);
  VectorFst<StdArc> *fst = VectorFst<StdArc>::Read(sample_machine);
  WFST_Trainer wfst_trainer = WFST_Trainer(fst,data,features,weights);
 
  wfst_trainer.write("untrained.fst");
  wfst_trainer.train();
  wfst_trainer.write("trained.fst");

}
