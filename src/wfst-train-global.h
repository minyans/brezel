// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//


#include "wfst-train.h"


class WFST_Trainer_Global : public WFST_Trainer {
 public:
  WFST_Trainer_Global();
  WFST_Trainer_Global(vector<LogVectorFst*> &fsts,
		      vector<int> &fst_unique_ids,
					 int num_unique_fsts,

		      
		      SymbolTable input_symbol_table,
		      SymbolTable output_symbol_table,
		      fsa_data data_formatted, 
		      Selector *s, Optimizer *optimizer,int num_arcs, 
		      int num_features, int symbol_bound, int num_threads, 
		      feature_map features,
		      feature_weights weights, bool debug_mode);

  /*
   * Overloads add_trainig_data to add the Feature Arc
   * Machines for the composition x_i o T in addition to x_i o T o y_i
   *
   * @param data_formatted - the formatted data
   */
  void add_training_data(fsa_data data_formatted);
    

  void train(int outer_max_iterations, double outer_threshold, int inner_max_iterations, 
	    double inner_threshold);


  /*
   * This is different from the version 
   * in wfst-train-local because we not 
   * do a per-state renormalization at the end 
   * It just iterates through the arcs and adds
   * a new weight that is the dot product 
   * of the weight vector and the feature vector
   * associated with that arc
   * 
   *
   *
   */
  void update_arc_weights(int fst_id, const column_vector &weights);
  void update_arc_weights(int fst_id);
  /**
   * This function wraps the computation of the gradient so that it can be 
   * passed to the dlib library for optimization. It calls gradient2, which
   * does the actual optimization
   *
   * @param weights computes the gradient for the current value of the weights
   *
   */
  const column_vector gradient(const column_vector &weights) const;


  double value(const column_vector &weights) const;
 private:
  
  
  //SHOUDL RENAME LATER
  fsa_data_features x_T_features;

   /**
   * This function does the actual computation of the gradient in parallel.
   * The number of threads in set in the constructor and handled through
   * static (???) fucntion. 
   *
   * @param weights the current vlaue of the weights
   *
   */
  const column_vector gradient2(const column_vector &weights);

 
  double value2(const column_vector &weights);

  /*
   * This takes an unnormalized FST 
   * It divides the weight on every accepting state
   * by the path sum. 
   *
   */
  void renormalize(LogVectorFst &fst);

  /*
   * Algorithm:
   * This function takes an FST fst and an FST with the 
   * same topology that has the feature id on each arc. 
   * It runs the forward algorithm on the FST to compute
   * the alpha values and the backward algorithm to compute
   * The beta values. Recall the marginal probability of cross
   * an arc_{i->j} is then \alpha_i \cdot w(arc_{i->j}) \cdot \beta_j.
   * We then add the fractional count to the count vector for each 
   * arc in the machine. 
   *
   * Usage:
   * This is a generalized method for computing both the 
   * observed and the expected counts for a globally normalized
   * WFST. These are basically the same algorithm so we generalized it. 
   * See the brezel technical report for more details.
   *
   * @param fst - a normalized fst
   * @param feature_fst - a FST with the same topology but with the original 
   * arc id as the weights
   */
  void calculate_counts(LogVectorFst &fst,
			VectorFst<FeatureArc> &feature_fst,
			vector<double> &counts);







  
};

