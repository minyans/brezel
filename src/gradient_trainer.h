// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//


#ifndef GRADIENT_TRAINER_H
#define GRADIENT_TRAINER_H
//DLIB
#include <dlib/optimization.h>

/* //taken from http://dlib.net/optimization_ex.cpp.html */
typedef dlib::matrix<double,0,1> column_vector;


class Gradient_Trainer {

 public:
   /**
   * This is a wrapper function for the DLIB optimization functions
   *
   * @param weights the current weight value
   *
   */
  virtual double value(const column_vector &weights) const {};

  /**
   * This function wraps the computation of the gradient so that it can be 
   * passed to the dlib library for optimization. It calls gradient2, which
   * does the actual optimization
   *
   * @param weights computes the gradient for the current value of the weights
   *
   */
  virtual  const column_vector gradient(const column_vector &weights) const {};


};


// wraps the value method the WFST Trainer
// so that a member function pointer can be called
// as if it were a function pointer
class Value
{
 public:

  Value (const Gradient_Trainer  *input) {
    trainer = input;
  }

  double operator() (const column_vector& arg) const {
    return trainer->value(arg);
  }

 private:
  const Gradient_Trainer *trainer;
};


// wraps the gradient computation of the WFST trainer
// so that a member function pointer can be called as
// if it were a function pointer
class Gradient {
 public:

  Gradient (const Gradient_Trainer *input) {
      trainer = input;
    }

  const column_vector operator() ( const column_vector& arg) const {
    return trainer->gradient(arg);
  }

 private:
  const Gradient_Trainer  *trainer;
};

#endif
