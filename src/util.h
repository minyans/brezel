// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//


//http://compbio.mit.edu/spimap/pub/spimap/src/common.h

// computes log(a + b) given log(a) and log(b)
inline double logadd(double lna, double lnb)
{
  if (lna == 1.0)
    return lnb;
  if (lnb == 1.0)
    return lna;
    
  double diff = lna - lnb;
  if (diff < 500.0)
    return log(exp(diff) + 1.0) + lnb;
  else
    return lna;
}


// computes log(a + b) given log(a) and log(b)
inline double logsub(double lna, double lnb)
{
  if (lna == 1.0)
    return lnb;
  if (lnb == 1.0)
    return lna;
    
  double diff = lna - lnb;
  if (diff < 500.0) {
    double diff2 = exp(diff) - 1.0;
    if (diff2 == 0.0)
      return -INFINITY;
    else
      return log(diff2) + lnb;
  } else
    return lna;
}


//computes the dot product of two vectors in real space
//first argument is ints
//second is double
inline double dot_product(vector<int> *a, vector<double> *b) {
  if (a->size() != b->size()) {
    //die
  }
  double total = 0.0;
  
  for (int i = 0; i < a->size(); ++i) {
    total += (*a)[i] * exp(-(*b)[i]);
  }
  return total;
}

