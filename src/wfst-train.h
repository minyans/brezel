// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#include <pthread.h>
#include <iostream>
#include <fstream>

#include <string>
#include <set>
#include <cmath>
#include <cfloat>
#include <stdlib.h>
#include <stddef.h>
#include <map>
#include <vector>
#include <time.h>
#include <algorithm>

//OpenFST
#include <fst/fstlib.h>
#include <fst/fst-decl.h>
#include <fst/map.h>

//Boost
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/ptr_container/ptr_vector.hpp>


//Glog
//#include <glog/logging.h>

#include "util.h"
#include "data-io.h"
#include "gradient_trainer.h"
#include "optimizer.h"
#include "selector.h"



using namespace fst;
using namespace std;
using namespace boost;

//this is a hack to get backpointers
typedef ExpectationWeight<LogWeight,LogWeight > FeatureWeight;
typedef ArcTpl<FeatureWeight> FeatureArc;

typedef WeightConvertMapper<LogArc, StdArc> Map_LS;
typedef WeightConvertMapper<StdArc, LogArc> Map_SL;


//type def common fst types
typedef VectorFst<LogArc> LogVectorFst;
typedef VectorFst<FeatureArc> FeatureVectorFst;


typedef pair<FeatureVectorFst, FeatureVectorFst > datum_formatted_features;
typedef vector<FeatureVectorFst >  fsa_data_features;


typedef pair<LogVectorFst, LogVectorFst > datum_formatted;
typedef vector<datum_formatted> fsa_data;



class WFST_Trainer : public Gradient_Trainer {
 protected:
  training_data data;

  Selector *s;
  Optimizer *optimizer;
  
  //formatted data and the features
  fsa_data data_formatted;
  fsa_data_features data_formatted_features;

  //feature map and feature weights
  feature_map features;
  feature_weights weights;

  //stats about the machine to be trained
  int num_arcs;
  int num_features;
  
  int symbol_bound;
  int num_threads;

  //whether to operate in debug mode
  bool debug_mode;

  //global members for tallying counts
  vector<double> observed_counts;
  vector<double> expected_counts;
  vector< vector<double> > expected_arc_traversals;


  //symbol_tables
  SymbolTable input_symbol_table;
  SymbolTable output_symbol_table;
  //fsts
  vector<FeatureVectorFst> feature_fsts;
  vector<LogVectorFst*> fsts;
  vector<int> fst_unique_ids;
  int num_unique_fsts;

  //functions
  double mult_features_by_weights(int arc_id);
  double mult_features_by_weights(int arc_id, const column_vector &weights);

  
  void add_training_data(fsa_data data_formatted);

 
  
  //calculates the expected counts
  virtual void calculate_observed_counts(int exemplar_num, FeatureVectorFst *fst, 
				LogVectorFst *fst_log,vector<LogWeight> *alpha, 
					 vector<LogWeight> *beta) {}
  /**
   * Calculates the expected counts
   */
  virtual void calculate_expected_counts(int exemplar_num) {} 

 public:

  //updates the weights
  //this is different for globally and locally normalized models
  virtual void update_arc_weights(int fst_id, const column_vector &weights) {
    cout << "ORIGINAL\n";
  }
  virtual void update_arc_weights(int fst_id) {}


  virtual void train(int outer_max_iterations, double outer_threshold, int inner_max_iterations, 
		    double inner_threshold) {}


  void set_optimization_algorithm(Optimizer *optimizer);


  
  void write(string file_out);
  static int get_number_arcs(LogVectorFst *fst);
  static void string_to_logfst(string str, LogVectorFst *fst,
			       SymbolTable *isyms, SymbolTable *osyms);
  
  static void string_to_stdfst(string str, StdVectorFst  *fst, 
			       const SymbolTable *syms);
  static string fst_to_string(StdVectorFst *fst);
  static void map_to_feature_arcs(LogVectorFst *fst, 
				 FeatureVectorFst *new_fst, bool zero);
  
};



