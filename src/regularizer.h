// Licensed  under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#ifndef REGULARIZER_H
#define REGULARIZER_H

#include <dlib/optimization.h>


class Regularizer {
 public:

		double value(const column_vector &weights) const {
			return dlib::dot(weights,weights);
		}
		column_vector gradient(const column_vector &weights) const {
			return 2* weights;
		}

};

#endif
