// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Liccense for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

#include "wfst-train-global.h"
/*
 * This is a wrapper for gradient2 in order to remove the const
 * so it can be called by the optimization library dlib
 *
 * @param weights - the weight vector where the gradient should be computed
 * @return the gradient value at that point
 */
const column_vector WFST_Trainer_Global::gradient(const column_vector &weights) const {
  return const_cast<WFST_Trainer_Global*>(this)->gradient2(weights);
}


/*
 * Computes the gardient at a given parameter setting theta
 * @param weights - the weight vector where the gradient should be computed
 * @return the gradient value at that point
 */
const column_vector WFST_Trainer_Global::gradient2(const column_vector &weights) {
  if (debug_mode) {
    cout << "GRAD START\n";
  }

  // number of training exemplars
  int N = this->data_formatted_features.size(); 
  update_arc_weights(weights);

  column_vector gradient(num_features);
  for (int f = 0; f < num_features; ++f) {
    gradient(f) = 0.0;
  }
  
  
    
    
    //reinitialize the observed and expectation counts
    for (int i = 0; i < num_arcs; ++i) {
      this->observed_counts[i] = -DBL_MAX;
      this->expected_counts[i] = -DBL_MAX;
    }
    
    //iterates over the exemplars. 
  
    //whether we've seen a fst id beore
    vector<bool> flags(num_unique_fsts);
    //the fst which are in this trianing batch
    vector<int> seen;

    vector<int> epoch;
    s->get_next_set(epoch);
  
    for (vector<int>::iterator it = epoch.begin(); it != epoch.end(); ++it) {
      int exemplar_num = *it;
      //cout << "EXEMPLAR NUM\t" << exemplar_num << "\n";
      //for (int exemplar_num = 0; exemplar_num < this->data_formatted.size(); ++exemplar_num) {

      if (flags[fst_unique_ids[exemplar_num]] == false) {
	flags[fst_unique_ids[exemplar_num]] = true;
	seen.push_back(fst_unique_ids[exemplar_num]);
	update_arc_weights(fst_unique_ids[exemplar_num],weights);

      }

    
      VectorFst<FeatureArc> exemplar_feature = data_formatted_features[exemplar_num];


      VectorFst<FeatureArc> x_T = x_T_features[exemplar_num];
      datum_formatted exemplar_log = this->data_formatted[exemplar_num];
      LogVectorFst medial_log, final_log;
            
      medial_log.SetInputSymbols(&input_symbol_table);
      medial_log.SetOutputSymbols(&input_symbol_table);
      final_log.SetInputSymbols(&output_symbol_table);
      final_log.SetOutputSymbols(&output_symbol_table);
      
      //cout<<"CREATING MEDIAL\n";
      Compose<LogArc>(exemplar_log.first,*fsts[exemplar_num],&medial_log);
      renormalize(medial_log);
      //cout<<"CREATING FINAL\n";
      Compose<LogArc>(medial_log,exemplar_log.second,&final_log);
      renormalize(final_log);

      //cout << "OBSERVED\n";
      calculate_counts(final_log, exemplar_feature, this->observed_counts);
      //cout << "EXPECTED\n";
      calculate_counts(medial_log, x_T, this->expected_counts);
      //WE NEED TWO FEATURE EXEMPLARS


    }

    for (int i = 0; i < num_features; ++i) {
      gradient(i)= exp(this->observed_counts[i]) -  exp(this->expected_counts[i]);
    }

    
    for (int i = 0; i < seen.size(); ++i) {
      update_arc_weights(seen[i]);
    }
    //for (int i = 0 ; i  < gradient.size(); ++i) {
    //    cout<<" "<<gradient(i);
    //}
    return gradient;


}


/*
 * This is the wrapper value2 the optimization library dlib so 
 * it can optimize a pointer to a member function
 *
 * @param weights - the parameters
 * @return the value of the function
 */
double WFST_Trainer_Global::value(const column_vector &weights) const {
  return const_cast<WFST_Trainer_Global*>(this)->value2(weights);
}

/*
 * This computes the function value at a given weight vector.
 * This should be parallelized by dividing the training examples
 * into subsets and doing each on a different thread.
 *
 * @param weights - the value of the weights
 * @param the value of the function
 *
 */
double WFST_Trainer_Global::value2(const column_vector &weights) {
  double likelihood = 0.0;
  //cout<<"UPDATING ARC WEIGHTS\n";

  for (int i = 0; i < fst_unique_ids.size(); ++i) {
    update_arc_weights(i,weights);
  }
   

  // ``filler'' FSTs to replace
  // the results from the composition
  // declared outside for loop for efficiency
  LogVectorFst medial;
  LogVectorFst final;
  
  medial.SetInputSymbols(&input_symbol_table);
  medial.SetOutputSymbols(&output_symbol_table);
  final.SetInputSymbols(&input_symbol_table);
  final.SetOutputSymbols(&output_symbol_table);

  int exemplar_num = 0;
  for (fsa_data::iterator it = this->data_formatted.begin(); it != this->data_formatted.end(); ++it) {
    datum_formatted exemplar = *it;
    //cout<<"COMPOSING MEDIAL in Value2\n";
    Compose<LogArc>(exemplar.first,*fsts[exemplar_num],&medial);
    medial.Write("medial.fst");
    //cout<<"COMPUTING FWD PROBS IN  MEDIAL\n";
    LogWeight tmp = ShortestDistance(medial);
    //cout<<"RENORMALIZING MEDIAL\n"; 
    renormalize(medial);
    //cout<<"COMPOSING FINAL\n";
    Compose<LogArc>(medial,exemplar.second,&final);
    final.Write("final.fst");
    
    //cout<<"COMPUTING FWD PROBS IN  FINAL\n";
    LogWeight expected_weight = ShortestDistance(final);
    //cout<<"COMPLETED FWD PROBS\n";
    likelihood -= expected_weight.Value();
    //cout<<"CALCULATED LL"<<likelihood<<endl;

    ++exemplar_num;
  }
  //cout<<"UPDATING ARC WEIGHTS\n";
  for (int i = 0; i < fst_unique_ids.size(); ++i) {
    update_arc_weights(i,weights);
  }
 
  //cout<<"COMPLETED UPDATE\n";
  return likelihood;
}



/*
 * Calculates the observed counts (without parallelization)
 *
 * @param exemplar_num - the training examplar number
 * @param fst - the FST in with FeatureArcs
 * @param fst_log - the FST with the actual weights in log space. This M in the pseudocode.
 * We assume this is already normalized
 * @param alpha - the alpha values
 * @param beta - the beta values
 *
 */
void WFST_Trainer_Global::calculate_counts(LogVectorFst &fst,
					  VectorFst<FeatureArc> &feature_fst, 
					  vector<double> &counts)
{

  vector<LogWeight> alpha,beta;
  ShortestDistance(fst,&alpha);
  ShortestDistance(fst,&beta, true);
  
  int arc_id = 0;
  for (StateIterator<LogVectorFst > siter(fst); 
       !siter.Done(); siter.Next()) {
    int state_id = siter.Value();

    //map of backpointers
    map<int,int> arc_mapper;
    int tmp = arc_id;
    
    //retrieves the backpointer through hacky magic
    for (ArcIterator<VectorFst<FeatureArc> > aiter1(feature_fst,state_id); 
	 !aiter1.Done(); aiter1.Next()) {
      const FeatureArc &arc = aiter1.Value();
      int old_arc_id = round(exp(-arc.weight.Value2().Value()));
      arc_mapper[tmp] = old_arc_id - 1;
      ++tmp;
    }

    for (ArcIterator<LogVectorFst > aiter2(fst,state_id); 
	 !aiter2.Done(); aiter2.Next()) {
      const LogArc &arc = aiter2.Value();
      int old_arc_id = arc_mapper[arc_id];
      LogWeight val = ((Times(Times(alpha[state_id],arc.weight.Value()), 
			      beta[arc.nextstate])));
   
      counts[old_arc_id] = logadd(counts[old_arc_id],-val.Value());
      ++arc_id;
    }
  }
}



void WFST_Trainer_Global::train(int outer_max_iterations, double outer_threshold, int inner_max_iterations, 
                               double inner_threshold) {
  
  int N = this->data_formatted_features.size(); // number of training exemplars
  
  //weights are stored in two different formats
  //this needs to be changed
  column_vector tmp_weights(num_features);
  for (int i = 0; i < num_features; ++i) {
    tmp_weights(i) = weights[i];
  }

  
  
  for (int iteration_num = 0; iteration_num < outer_max_iterations; ++iteration_num) {
    //E - STEP
    Value value(this);
    Gradient gradient(this);
    
    cout << "OUTER ITERATION: " << iteration_num << "\n";
    //update_arc_weights(tmp_weights);
    
    
    //reinitialize the observed and expectation counts
    for (int i = 0; i < num_arcs; ++i) {
      this->observed_counts[i] = -DBL_MAX;
      this->expected_counts[i] = -DBL_MAX;
    }
    
    //iterates over the exemplars. 
  
    vector<int> epoch;
    s->get_next_set(epoch);
  
    for (vector<int>::iterator it = epoch.begin(); it != epoch.end(); ++it) {
      int exemplar_num = *it;
      cout << "EXEMPLAR NUM\t" << exemplar_num << "\n";
      //for (int exemplar_num = 0; exemplar_num < this->data_formatted.size(); ++exemplar_num) {
      
      VectorFst<FeatureArc> exemplar_feature = data_formatted_features[exemplar_num];


      VectorFst<FeatureArc> x_T = x_T_features[exemplar_num];
      datum_formatted exemplar_log = this->data_formatted[exemplar_num];
      LogVectorFst medial_log, final_log;
            
      medial_log.SetInputSymbols(&input_symbol_table);
      medial_log.SetOutputSymbols(&input_symbol_table);
      final_log.SetInputSymbols(&output_symbol_table);
      final_log.SetOutputSymbols(&output_symbol_table);
      
      
  
      cout << "STARTING FIRST COMPOSITION" << endl;
      Compose<LogArc>(exemplar_log.first,*fsts[exemplar_num],&medial_log);
 
      cout << "RENORMALIZE MEDIAL LOG" << endl;
      medial_log.Write("medial_log.fst");
      renormalize(medial_log);
      
      cout << "STARTING SECOND COMPOSITION" << endl;
      Compose<LogArc>(medial_log,exemplar_log.second,&final_log);
      cout << "RENORMALIZE FINAL LOG" << endl;
      renormalize(final_log);

      cout << "OBSERVED\n";
      calculate_counts(final_log, exemplar_feature, this->observed_counts);
      cout << "EXPECTED\n";
      calculate_counts(medial_log, x_T, this->expected_counts);
      //WE NEED TWO FEATURE EXEMPLARS


    }
    this->optimizer->optimize(value,gradient,tmp_weights);
    //M - step
    update_arc_weights(tmp_weights);
    
    //exit(0);
  }
}


/*
 *
 *
 */
void WFST_Trainer_Global::add_training_data(fsa_data data_formatted) {
  cout << "HERE1/2\n";
  WFST_Trainer::add_training_data(data_formatted);
  x_T_features.clear();

  cout << "HERE1\n";
  int i = 0; 
  for (fsa_data::iterator it = this->data_formatted.begin(); 
       it != this->data_formatted.end(); ++it) {
    datum_formatted exemplar = *it;

    cout << "HERE2\n";
    VectorFst<FeatureArc> fst_upper_features;
        
    fst_upper_features.SetInputSymbols(&input_symbol_table);
    fst_upper_features.SetOutputSymbols(&input_symbol_table);
    
    map_to_feature_arcs(&exemplar.first,&fst_upper_features,true);

    VectorFst<FeatureArc> medial_features;
    
    medial_features.SetInputSymbols(&input_symbol_table);
    medial_features.SetOutputSymbols(&output_symbol_table);
    Compose<FeatureArc>(fst_upper_features,feature_fsts[i],&medial_features);
        
    x_T_features.push_back(medial_features);
    ++i;
  }
  
}


/*
 * Renormalizes the distribution by 
 * dividing the final state (s)??? 
 * by the sume of all paths
 */
void WFST_Trainer_Global::renormalize(LogVectorFst &fst) {
  LogWeight pathsum = ShortestDistance(fst);
  
  for (StateIterator<LogVectorFst > siter(fst); !siter.Done(); siter.Next()) {
    int state_id = siter.Value();
  
    if (fst.Final(state_id) != LogWeight::Zero()) {
      LogWeight newWeight = Divide(fst.Final(state_id),pathsum);
      fst.SetFinal(state_id,newWeight);
    }
  }
}

//STUB
void WFST_Trainer_Global::update_arc_weights(int fst_id) {
  column_vector tmp(this->weights.size());
  for (int i = 0; i < this->weights.size(); ++i) {
    tmp(i) = weights[i];
  }
  //cout<<"CALLING UPDATE WITH TMP WEIGHTS\n";
  update_arc_weights(fst_id,tmp);
}

/*
 *
 *
 *
 */

void WFST_Trainer_Global::update_arc_weights(int fst_id, const column_vector &weights) {
  
  int arc_id = 0;
  for (StateIterator<LogVectorFst > siter(*this->fsts[fst_id]); !siter.Done(); siter.Next()) {
    int state_id = siter.Value();
    
    for (MutableArcIterator<LogVectorFst > aiter(this->fsts[fst_id],state_id); 
	 !aiter.Done(); aiter.Next()) {
      const LogArc &cur_arc = aiter.Value();
      double new_weight = -mult_features_by_weights(arc_id,weights);
      LogArc arc = LogArc(cur_arc.ilabel,cur_arc.olabel,new_weight,cur_arc.nextstate);
      aiter.SetValue(arc);
      ++arc_id;
    }
  }

}

/**
  This constructor takes no arguments and initializes nothing. It should never be 
  used except when using DLIB's wrapper class for the optimization. 
  */
WFST_Trainer_Global::WFST_Trainer_Global() {

}

/**
 * This is the main constructor the WFST Trainer. 
 *
 * @param fst the VectorFst to train
 * @param data_formatted the formatted training data
 * @param num_arcs the number of arcs in the machine
 * @param num_features the number of features in the machine
 * @param symbol_bound an upper bound on the largest symbol id in the symbol table 
 * @param features a map from the arc ids to the features that fire on that arc
 * @param weights the initial weights for all the features
 */
WFST_Trainer_Global::WFST_Trainer_Global(vector<LogVectorFst*> &fsts,
					 vector<int> &fst_unique_ids,
					 int num_unique_fsts,
					 SymbolTable input_symbol_table,
					 SymbolTable output_symbol_table,
fsa_data data_formatted,
					 Selector *s, Optimizer *optimizer,
					int num_arcs, int num_features, int symbol_bound, 
					int num_threads, feature_map features,feature_weights weights, 
					bool debug_mode) {
 
  this->s = s;
  this->optimizer = optimizer;

  this->fsts = fsts;
  this->fst_unique_ids = fst_unique_ids;
  this->num_unique_fsts;

  this->input_symbol_table = input_symbol_table;
  this->output_symbol_table = output_symbol_table;

  
  this->data = data;
  this->weights = weights;
  this->features = features;
  

  this->num_arcs = num_arcs;
  this->num_features = num_features;
  
  this->symbol_bound = symbol_bound;
  this->num_threads = num_threads;
  
  this->debug_mode = debug_mode;
  
  observed_counts.resize(num_arcs);
  expected_counts.resize(num_arcs);
  expected_arc_traversals.resize(data_formatted.size());
  
  cout<<"update arc weights of simple machine"<<endl;
  for (int i = 0; i < fst_unique_ids.size(); ++i) {
    update_arc_weights(fst_unique_ids[i]);
  }
  
  cout<<"update arc weights of simple machine done"<<endl;
  for (int i = 0; i < fsts.size(); ++i) { 
    VectorFst<FeatureArc> fst_feature;
    fst_feature.SetInputSymbols(&input_symbol_table);
     fst_feature.SetOutputSymbols(&output_symbol_table);
 
     map_to_feature_arcs(fsts[i],&fst_feature,false);	
     fst_feature.Write("test.fst");
    this->feature_fsts.push_back(fst_feature);
    this->feature_fsts[i].Write("test2.fst");
  }
  cout << "ADD TRAINING DATA\n";
  this->add_training_data(data_formatted);
  cout << "CONSTRUCTOR ENDED\n";
  
}
