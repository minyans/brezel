// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Author: ryan.cotterell@gmail.com (Ryan Cotterell)
//

/*
 compose_benchmark/contextEdit_logsemiring.fst
compose_benchmark/lower_expectation.fst
compose_benchmark/lower_logsemiring.fst
compose_benchmark/observed_expectaton.fst
compose_benchmark/upper_expectation.fst
compose_benchmark/upper_logsemiring.fst
*/


#include "wfst-train-local-fast.h"

int main(int argc, char* argv[]) {
  string sample_machine = argv[1];
  // string sample_symbol_table = argv[2];

  FeatureReader fr = FeatureReader(argv[2]);
  //WeightReader wr = WeightReader(argv[4]);

  feature_map features = fr.get_features();
  // feature_weights weights = wr.get_weights();

  int exemplars = 4;
  vector<string> upper_fst(exemplars);

  upper_fst[0] = "violet_data/surface_passUp0.fst"; 
  upper_fst[1] = "violet_data/surface_passUp1.fst"; 
  upper_fst[2] = "violet_data/surface_passUp2.fst";
  upper_fst[3] = "violet_data/surface_passUp3.fst";
  
  
  vector<string> lower_fst(exemplars);

  
  lower_fst[0] = "violet_data/surfaceEdit_passUp0.fst";
  lower_fst[1] = "violet_data/surfaceEdit_passUp1.fst"; 
  lower_fst[2] = "violet_data/surfaceEdit_passUp2.fst";
  lower_fst[3] = "violet_data/surfaceEdit_passUp3.fst";


  VectorFst<LogArc> *fst = VectorFst<LogArc>::Read(sample_machine);
  

  vector<datum_formatted> ps(exemplars);


  for (int i = 0; i < exemplars; ++i) {
    VectorFst<LogArc> *upper_in = VectorFst<LogArc>::Read(upper_fst[i]);
    VectorFst<StdArc> *lower_in = VectorFst<StdArc>::Read(lower_fst[i]);
    VectorFst<LogArc> lower;
    ArcMap(*lower_in,&lower,Map_SL());
    datum_formatted p(*upper_in,lower);
    ps[i] = p;
  }


  feature_weights weights;
  int num_features = 16;
  for (int i = 0; i < num_features; ++i) {
    weights.push_back(2 * i);
  }


  WFST_Trainer *wfst_trainer = new WFST_Trainer_Local(fst,ps,16, num_features, 100, 4, features,weights,true);

  wfst_trainer->write("untrained.fst");
  wfst_trainer->train(5,1e-1,1,1e-1);
  wfst_trainer->write("trained.fst");


}
